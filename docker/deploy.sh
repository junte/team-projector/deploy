#! /bin/bash

set -o errexit
set -o nounset


docker network create team_projector || true

source <(sed -E -n 's/[^#]+/export &/ p' ./config/db.env)

export IMAGE_TAG=$(cat version)
export COMPOSE_PROJECT_NAME=team_projector

docker-compose up -d
