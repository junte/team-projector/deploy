#! /bin/sh

set -o errexit

HASHED_PASSWORD=$(caddy hash-password --plaintext ${DJANGO_SUPERUSER_PASSWORD})
export BASIC_PASSWORD=${HASHED_PASSWORD}
export BASIC_USER=${DJANGO_SUPERUSER_LOGIN}

caddy run --config /etc/caddy/Caddyfile --adapter caddyfile
