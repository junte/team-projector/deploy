#! /bin/bash

set -o errexit
set -o nounset

source <(sed -E -n 's/[^#]+/export &/ p' ./config/db.env)

docker run \
    -e PGHOST=postgres \
    -e PGUSER=${DJANGO_DATABASE_USER} \
    -e PGPASSWORD=${DJANGO_DATABASE_PASSWORD} \
    -e BACKUP_DATABASES=${DJANGO_DATABASE_NAME} \
    --network team_projector \
    -v /var/backups/db:/pg_backup \
    junte/pg-backup:1.0-client13
