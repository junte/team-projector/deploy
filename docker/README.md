# Deploying via docker-compose

1. Prepare configs:
    1. Prepare env files in `config` folder: rename from `.env.example` to `.env`
    2. Edit renamed files for custom settings
2. Login with provided credentials to gitlab registry: `docker login registry.gitlab.com`
3. Run deploy script: `./deploy.sh`